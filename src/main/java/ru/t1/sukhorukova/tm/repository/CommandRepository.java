package ru.t1.sukhorukova.tm.repository;

import ru.t1.sukhorukova.tm.constant.ArgumentConst;
import ru.t1.sukhorukova.tm.constant.CommandConst;
import ru.t1.sukhorukova.tm.model.Command;

public class CommandRepository {

    public static final Command CMD_HELP = new Command(CommandConst.CMD_HELP, ArgumentConst.CMD_HELP, "Show help.");
    public static final Command CMD_VERSION = new Command(CommandConst.CMD_VERSION, ArgumentConst.CMD_VERSION, "Show version.");
    public static final Command CMD_ABOUT = new Command(CommandConst.CMD_ABOUT, ArgumentConst.CMD_ABOUT, "Show about developer.");
    public static final Command CMD_INFO = new Command(CommandConst.CMD_INFO, ArgumentConst.CMD_INFO, "Show system information.");
    public static final Command CMD_ARGUMENT = new Command(CommandConst.CMD_ARGUMENT, ArgumentConst.CMD_ARGUMENT, "Show argument list.");
    public static final Command CMD_COMMAND = new Command(CommandConst.CMD_COMMAND, ArgumentConst.CMD_COMMAND, "Show command list.");
    public static final Command CMD_EXIT = new Command(CommandConst.CMD_EXIT, null, "Close application.");

    public static final Command[] TERMINAL_COMMANDS = {CMD_HELP, CMD_VERSION, CMD_ABOUT, CMD_INFO, CMD_ARGUMENT, CMD_COMMAND, CMD_EXIT};

    public static Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
