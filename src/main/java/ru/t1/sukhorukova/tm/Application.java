package ru.t1.sukhorukova.tm;

import ru.t1.sukhorukova.tm.constant.ArgumentConst;
import ru.t1.sukhorukova.tm.constant.CommandConst;
import ru.t1.sukhorukova.tm.model.Command;
import ru.t1.sukhorukova.tm.repository.CommandRepository;
import ru.t1.sukhorukova.tm.util.FormatUtil;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        processArguments(args);
        System.out.println("** WELCOME TASK MANAGER **");
        final Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private static void processArguments(String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        System.exit(0);
    }

    private static void processArgument(String command) {
        switch (command) {
            case ArgumentConst.CMD_VERSION:
                showVersion();
                break;
            case ArgumentConst.CMD_ABOUT:
                showAbout();
                break;
            case ArgumentConst.CMD_HELP:
                showHelp();
                break;
            case ArgumentConst.CMD_INFO:
                showInfo();
                break;
            case ArgumentConst.CMD_ARGUMENT:
                showArguments();
                break;
            case ArgumentConst.CMD_COMMAND:
                showCommands();
                break;
            default:
                showArgumentError();
                break;
        }
    }

    private static void processCommand(String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case CommandConst.CMD_VERSION:
                showVersion();
                break;
            case CommandConst.CMD_ABOUT:
                showAbout();
                break;
            case CommandConst.CMD_HELP:
                showHelp();
                break;
            case CommandConst.CMD_INFO:
                showInfo();
                break;
            case CommandConst.CMD_EXIT:
                exit();
                break;
            case CommandConst.CMD_ARGUMENT:
                showArguments();
                break;
            case CommandConst.CMD_COMMAND:
                showCommands();
                break;
            default:
                showCommandError();
                break;
        }
    }

    private static void showInfo() {
        System.out.println("[INFO]");

        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usageMemory = totalMemory - freeMemory;

        final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        final String usageMemoryFormat = FormatUtil.formatBytes(usageMemory);

        final String maxMemoryValue = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryFormat;

        System.out.println("Available processors: " + availableProcessors + " cores");
        System.out.println("Free memory: " + freeMemoryFormat);
        System.out.println("Maximum memory: " + maxMemoryValue);
        System.out.println("Total memory: " + totalMemoryFormat);
        System.out.println("Usage memory: " + usageMemoryFormat);
    }

    private static void exit() {
        System.exit(0);
    }

    private static void showArgumentError() {
        System.out.println("[ERROR]");
        System.out.println("This argument not supported...");
        System.exit(1);
    }

    private static void showCommandError() {
        System.out.println("[ERROR]");
        System.out.println("This command not supported...");
        System.exit(1);
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.8.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Victoria Sukhorukova");
        System.out.println("vsukhorukova@t1-consulting.ru");
    }

    private static void showArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = CommandRepository.getTerminalCommands();
        for (final Command command: commands) {
            if (command == null) continue;
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    private static void showCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = CommandRepository.getTerminalCommands();
        for (final Command command: commands) {
            if (command == null) continue;
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = CommandRepository.getTerminalCommands();
        for (final Command command: commands) System.out.println(command);
    }

}
